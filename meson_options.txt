option('introspection', type : 'boolean', value : true, description : 'GObject Introspection generation')
option('docs', type : 'boolean', value : true, description : 'Vala Documentation generation')
