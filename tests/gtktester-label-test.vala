/*
 * gsvg-test.vala

 * Copyright (C) 2017 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

class GSvgTest.Suite : Object
{
    static int main (string[] args)
    {
        GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
        Test.init (ref args);
        Test.add_func ("/gtktester/label",
        ()=>{
            Gtk.init ();
            var w = new Gtk.Label ("THIS IS A LABEL UNDER TEST");
            var app = new Gtkt.Application ();
            app.initialize_widget.connect (()=>{
                 app.add_test ("Test Simple Label Widget", "Demo test for a known Widget");
                 app.set_widget (w);
            });

            app.initialize.connect (()=>{
                message ("Initializing the test");
            });

            app.finished.connect (()=>{
                message ("Finishing tests");
                app.quit ();
            });

            app.run ();
        });
        return Test.run ();
    }
}
