/*
 * gsvg-test.vala

 * Copyright (C) 2017 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

class GSvgTest.Suite : Object
{
    public class MyWidget : Gtk.Box {
        public MyWidget () {
            Object (orientation: Gtk.Orientation.VERTICAL, spacing: 5);
        }

        construct {
            var l = new Gtk.Label ("TOP LABEL");
            append (l);
            for (int i = 0; i < 50; i++) {
                var b = new Gtk.Box (Gtk.Orientation.HORIZONTAL, 10);
                for (int j = 0; j < 10; j++) {
                    var lh = new Gtk.Label ("HORIZONTAL LABEL %d".printf (j));
                    b.append (lh);
                }
                append (b);
                var lv = new Gtk.Label ("LABEL %d".printf (i));
            }
        }
    }

    static int main (string[] args)
    {
        GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
        Test.init (ref args);
        Test.add_func ("/gtktester/label",
        ()=>{
            Gtk.init ();

            var w = new MyWidget ();
            var app = new Gtkt.Application ();
            app.timeout = 10;
            app.initialize_widget.connect (()=>{
                app.add_test ("Test Simple Label Widget", "Demo test for a known Widget");
                app.set_widget (w);
            });

            app.initialize.connect (()=>{
                message ("Initializing the test");
            });

            app.finished.connect (()=>{
                message ("Finishing tests");
                app.quit ();
            });

            app.run ();
        });
        return Test.run ();
    }
}
