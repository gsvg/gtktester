/* gtktester.vala
 *
 * Copyright (C) 2017 Daniel Espinosa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Gtk;

internal errordomain WindowError {
    DUPLICATED_TEST_CASE_ERROR
}

/**
 * Main window to execute tests on.
 *
 * Createa a new {@link WindowTester} then set your
 * widget using {@link widget} to test on. The
 * window will show the widget, so you can:
 *
 *  1. Verify if your widget shows correctly
 *  1. Interact with your widget if you set {@link waiting_for_event} to true
 *  1. Connect to {@link initialize} signal in order to set any data to your widget before it is shown
 */
internal class Gtkt.Window : Gtk.ApplicationWindow {
    private Gtk.TextView textview;
    private Gtk.TextBuffer description;
    private Gtk.Box   widget_box;
    private Gtk.ListBox   list_box;
    private Gtk.Widget _widget;
    private Gtkt.HeaderBar headerbar;

    public void set_widget (Gtk.Widget w) {
        if (_widget != null) {
            widget_box.remove (_widget);
        }

        _widget = w;
        widget_box.append (_widget);
        debug ("Appended Widget under test");
    }

    construct {
        headerbar = new Gtkt.HeaderBar ();
        headerbar.next.connect (()=>{
            application.activate_action ("next", null);
        });
        this.set_titlebar (headerbar);
        var b = new Gtk.Box (Gtk.Orientation.VERTICAL, 0);
        this.child = b;
        list_box = new Gtk.ListBox ();
        b.append (list_box);
        var sw = new Gtk.ScrolledWindow ();
        description = new Gtk.TextBuffer (null);
        textview = new Gtk.TextView.with_buffer (description);
        textview.editable = false;
        sw.child = textview;
        b.append (sw);
        sw.hexpand = true;
        var box = new Gtk.Box (Gtk.Orientation.VERTICAL, 0);
        var l = new Gtk.Label ("Widget under test:");
        box.append (l);
        var swb = new Gtk.ScrolledWindow ();
        widget_box = new Gtk.Box (Gtk.Orientation.VERTICAL, 0);
        swb.child = widget_box;
        swb.hexpand = true;
        swb.vexpand = true;
        box.append (swb);
        box.hexpand = true;
        box.vexpand = true;
        b.append (box);
        b.hexpand = true;
        b.vexpand = true;
    }

    public Window (Gtk.Application app) {
        GLib.Object (application: app);
    }

    public void set_test_name (string name) {
        headerbar.title = name;
    }

    public void set_nfails (int n) {
        headerbar.fails_number = n;
    }

    public void set_status (Gtkt.Status status) {
        headerbar.status = status;
    }

    public void set_description (string txt) {
        description.set_text (txt);
    }
}
